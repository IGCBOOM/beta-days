package com.legacy.betadays.client;

import com.legacy.betadays.BDConfig;
import com.legacy.betadays.client.gui.ClassicMainMenuScreen;
import com.legacy.betadays.client.gui.LoadingDimensionsScreen;

import net.minecraft.SharedConstants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.client.event.RegisterGuiOverlaysEvent;
import net.minecraftforge.client.event.RenderGuiOverlayEvent;
import net.minecraftforge.client.event.ScreenEvent;
import net.minecraftforge.client.event.ViewportEvent;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.client.gui.overlay.ForgeGui;
import net.minecraftforge.client.gui.overlay.VanillaGuiOverlay;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@SuppressWarnings("resource")
public class BetaClientEvents
{
	private static int prevDimension = 0;

	@SubscribeEvent
	public static void onOpenGui(ScreenEvent.Opening event)
	{
		if (mc().player != null && event.getScreen() instanceof ReceivingLevelScreen && BDConfig.CLIENT.customDimensionMessages())
		{
			LoadingDimensionsScreen guiEnterEnd = new LoadingDimensionsScreen(false, false);
			LoadingDimensionsScreen guiEnterNether = new LoadingDimensionsScreen(true, false);

			LoadingDimensionsScreen guiLeaveEnd = new LoadingDimensionsScreen(false, true);
			LoadingDimensionsScreen guiLeaveNether = new LoadingDimensionsScreen(true, true);

			if (mc().player.level.dimension() != Level.OVERWORLD) // not overworld
			{
				if (mc().player.level.dimension() == Level.NETHER) // is nether
				{
					event.setNewScreen(guiLeaveNether);
					prevDimension = -1;
				}

				if (mc().player.level.dimension() == Level.END)
				{
					event.setNewScreen(guiLeaveEnd);
					prevDimension = 1;
				}
			}
			else if (mc().player.level.dimension() == Level.OVERWORLD) // is overworld
			{
				if (prevDimension == -1)
				{
					event.setNewScreen(guiEnterNether);
					prevDimension = 0;
				}

				if (prevDimension == 1)
				{
					event.setNewScreen(guiEnterEnd);
					prevDimension = 0;
				}
			}
		}

		if (BDConfig.CLIENT.enableClassicMenu() && event.getScreen() != null && event.getScreen().getClass() == TitleScreen.class && !mc().isDemo())
			event.setNewScreen(new ClassicMainMenuScreen(false));
	}

	@SubscribeEvent
	public static void onFogRender(ViewportEvent.RenderFog event)
	{
		Level level = mc().level;

		if (level != null && level.isClientSide)
		{
			if (BDConfig.CLIENT.disableNetherFog() && level.dimension().equals(Level.NETHER))
			{
				event.scaleFarPlaneDistance(5.0F);
				event.scaleNearPlaneDistance(5.5F);
				event.setCanceled(true);
			}
		}

	}

	@SubscribeEvent
	public static void onSoundPlayed(PlaySoundEvent event)
	{
		if (BDConfig.CLIENT.disableCombatSounds() && event.getSound() != null)
		{
			ResourceLocation sound = event.getSound().getLocation();

			if (sound == SoundEvents.PLAYER_ATTACK_NODAMAGE.getLocation() || sound == SoundEvents.PLAYER_ATTACK_WEAK.getLocation() || sound == SoundEvents.PLAYER_ATTACK_STRONG.getLocation() || sound == SoundEvents.PLAYER_ATTACK_CRIT.getLocation() || sound == SoundEvents.PLAYER_ATTACK_SWEEP.getLocation() || sound == SoundEvents.PLAYER_ATTACK_KNOCKBACK.getLocation())
				event.setSound(null);
		}
	}

	private static int rows = 1;

	@SubscribeEvent
	public static void renderOverlayPre(RenderGuiOverlayEvent.Pre event)
	{
		ResourceLocation id = event.getOverlay().id();
		boolean armor = id.equals(VanillaGuiOverlay.ARMOR_LEVEL.id());

		if (BDConfig.SERVER.disableExperienceDrop())
		{
			if (id.equals(VanillaGuiOverlay.EXPERIENCE_BAR.id()))
			{
				event.setCanceled(true);
			}

		}

		if (BDConfig.SERVER.hungerDisabled())
		{
			if (armor && mc().gui instanceof ForgeGui gui)
			{
				Player player = gui.getMinecraft().player;

				if (player != null)
				{
					int offset = 10 * (rows = healthRows(player, gui));
					event.getPoseStack().translate(101F, offset, 0);
				}
			}

			if (id.equals(VanillaGuiOverlay.FOOD_LEVEL.id()))
				event.setCanceled(true);

		}

		if (mc().gui instanceof ForgeGui gui)
		{
			if (id.equals(VanillaGuiOverlay.PLAYER_HEALTH.id()))
			{

				if (BDConfig.SERVER.hungerDisabled())
					gui.rightHeight += 3;

				if (BDConfig.SERVER.disableExperienceDrop())
				{
					gui.leftHeight -= 7;

					if (gui.getMinecraft().player.isRidingJumpable())
						gui.leftHeight += 7;
				}
			}

			if (id.equals(VanillaGuiOverlay.MOUNT_HEALTH.id()) && gui.getMinecraft().player.isUnderWater() && (gui.getMinecraft().player.isPassenger() || !BDConfig.SERVER.disableExperienceDrop()))
			{
				gui.rightHeight += 7;
			}
		}
	}

	@SubscribeEvent
	public static void renderOverlayPost(RenderGuiOverlayEvent.Post event)
	{
		ResourceLocation id = event.getOverlay().id();
		boolean armor = id.equals(VanillaGuiOverlay.ARMOR_LEVEL.id());

		if (BDConfig.SERVER.hungerDisabled())
		{
			if (armor && mc().player != null)
				event.getPoseStack().translate(-101F, -(10 * rows), 0);
		}
	}

	private static int healthRows(Player player, Gui gui)
	{
		AttributeInstance attrMaxHealth = player.getAttribute(Attributes.MAX_HEALTH);
		float healthMax = Math.max((float) attrMaxHealth.getValue(), Math.max(gui.lastHealth, gui.displayHealth));
		int absorb = Mth.ceil(player.getAbsorptionAmount());

		return Mth.ceil((healthMax + absorb) / 2.0F / 10.0F);
	}

	public static void registerOverlays(RegisterGuiOverlaysEvent event)
	{
		event.registerAboveAll("beta_days_old_version", (gui, pose, partialTicks, screenWidth, screenHeight) ->
		{
			Minecraft mc = gui.getMinecraft();
			if (BDConfig.CLIENT.displayIngameVersion() && !mc.options.hideGui && !mc.options.renderDebug)
				mc.font.drawShadow(pose, "Minecraft Release " + SharedConstants.getCurrentVersion().getName(), 2, 2, -1);
		});
	}

	private static Minecraft mc()
	{
		return Minecraft.getInstance();
	}
}