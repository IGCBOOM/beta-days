package com.legacy.betadays.client.gui;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.legacy.betadays.BetaDays;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;

import net.minecraft.ChatFormatting;
import net.minecraft.SharedConstants;
import net.minecraft.Util;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.AccessibilityOptionsScreen;
import net.minecraft.client.gui.screens.LanguageSelectScreen;
import net.minecraft.client.gui.screens.OptionsScreen;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.client.gui.screens.multiplayer.JoinMultiplayerScreen;
import net.minecraft.client.gui.screens.packs.PackSelectionScreen;
import net.minecraft.client.gui.screens.worldselection.SelectWorldScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackRepository;
import net.minecraft.util.Mth;
import net.minecraftforge.client.gui.ModListScreen;

public class ClassicMainMenuScreen extends TitleScreen
{
	private static final ResourceLocation MINECRAFT_TITLE_TEXTURES = new ResourceLocation("textures/gui/title/minecraft.png");
	private static final ResourceLocation ACCESSIBILITY_TEXTURES = new ResourceLocation("textures/gui/accessibility.png");
	private static final ResourceLocation FORGE_BUTTON = BetaDays.locate("textures/gui/forge.png");
	private final boolean showFadeInAnimation;
	private String splashText;
	private int widthCopyright;
	private int widthCopyrightRest;
	private long firstRenderTime;

	public ClassicMainMenuScreen()
	{
		this(false);
	}

	public ClassicMainMenuScreen(boolean fadeIn)
	{
		this.showFadeInAnimation = fadeIn;
	}

	@Override
	protected void init()
	{
		if (this.splashText == null)
		{
			this.splashText = this.minecraft.getSplashManager().getSplash();
		}

		this.widthCopyright = this.font.width("Copyright Mojang AB. Do not distribute.");
		this.widthCopyrightRest = this.width - this.widthCopyright - 2;
		int j = this.height / 4 + 48;
		this.addSingleplayerMultiplayerButtons(j, 24);

		this.addRenderableWidget(new ImageButton(this.width - 25, 3 * 2, 20, 20, 0, 0, 20, FORGE_BUTTON, 32, 64, (onPress) ->
		{
			this.minecraft.setScreen(new ModListScreen(this));
		}));

		this.addRenderableWidget(new ImageButton(this.width - 25, 3 * 18, 20, 20, 0, 106, 20, Button.WIDGETS_LOCATION, 256, 256, (onPress) ->
		{
			this.minecraft.setScreen(new LanguageSelectScreen(this, this.minecraft.options, this.minecraft.getLanguageManager()));
		}, Component.translatable("narrator.button.language")));
		this.addRenderableWidget(new Button(this.width / 2 - 100, j + (24 * 2), 200, 20, Component.translatable("menu.options"), (onPress) ->
		{
			this.minecraft.setScreen(new OptionsScreen(this, this.minecraft.options));
		}));
		this.addRenderableWidget(new Button(this.width / 2 - 100, j + (24 * 3), 200, 20, Component.translatable("beta.menu.modsTextures"), (onPress) ->
		{
			this.minecraft.setScreen(new PackSelectionScreen(this, this.minecraft.getResourcePackRepository(), this::updatePackList, this.minecraft.getResourcePackDirectory(), Component.translatable("resourcePack.title")));
		}));
		this.addRenderableWidget(new ImageButton(this.width - 25, 3 * 10, 20, 20, 0, 0, 20, ACCESSIBILITY_TEXTURES, 32, 64, (onPress) ->
		{
			this.minecraft.setScreen(new AccessibilityOptionsScreen(this, this.minecraft.options));
		}, Component.translatable("narrator.button.accessibility")));

		this.minecraft.setConnectedToRealms(false);
	}

	@Override
	public void render(PoseStack stack, int mouseX, int mouseY, float partialTicks)
	{
		if (this.firstRenderTime == 0L && this.showFadeInAnimation)
		{
			this.firstRenderTime = Util.getMillis();
		}

		float f = this.showFadeInAnimation ? (float) (Util.getMillis() - this.firstRenderTime) / 1000.0F : 1.0F;
		fill(stack, 0, 0, this.width, this.height, -1);
		int j = this.width / 2 - 137;

		RenderSystem.enableBlend();
		RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, this.showFadeInAnimation ? (float) Mth.ceil(Mth.clamp(f, 0.0F, 1.0F)) : 1.0F);
		blit(stack, 0, 0, this.width, this.height, 0.0F, 0.0F, 16, 128, 16, 128);
		float f1 = this.showFadeInAnimation ? Mth.clamp(f - 1.0F, 0.0F, 1.0F) : 1.0F;
		int l = Mth.ceil(f1 * 255.0F) << 24;

		if ((l & -67108864) != 0)
		{
			this.renderBackground(null, 0);
			RenderSystem.setShader(GameRenderer::getPositionTexShader);
			RenderSystem.setShaderTexture(0, MINECRAFT_TITLE_TEXTURES);
			RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, f1);
			blit(stack, j + 0, 30, 0, 0, 155, 44);
			blit(stack, j + 155, 30, 0, 45, 155, 44);

			if (this.splashText != null)
			{
				stack.pushPose();
				stack.translate((float) (this.width / 2 + 90), 70.0F, 0.0F);
				stack.mulPose(Vector3f.ZP.rotationDegrees(-20.0F));
				float f2 = 1.8F - Mth.abs(Mth.sin((float) (Util.getMillis() % 1000L) / 1000.0F * ((float) Math.PI * 2F)) * 0.1F);
				f2 = f2 * 100.0F / (float) (this.font.width(this.splashText) + 32);
				stack.scale(f2, f2, f2);
				drawCenteredString(stack, this.font, this.splashText, 0, -8, 16776960 | l);
				stack.popPose();
			}

			String s = "Minecraft " + SharedConstants.getCurrentVersion().getName();
			s = s + ("release".equalsIgnoreCase(this.minecraft.getVersionType()) ? "" : "/" + this.minecraft.getVersionType());

			String releaseType = this.minecraft.getVersionType();
			drawString(stack, this.font, ChatFormatting.DARK_GRAY + "Minecraft " + releaseType.replace("r", "R") + " " + SharedConstants.getCurrentVersion().getName(), 2, 3 * 2, 16777215 | l);
			drawString(stack, this.font, "Copyright Mojang AB. Do not distribute.", this.widthCopyrightRest, this.height - 10, 16777215 | l);

			for (GuiEventListener guieventlistener : this.children())
			{
				if (guieventlistener instanceof AbstractWidget widget)
					widget.setAlpha(f1);
			}
		}

		for (int r = 0; r < this.renderables.size(); ++r)
		{
			this.renderables.get(r).render(stack, mouseX, mouseY, partialTicks);
		}

	}

	private void addSingleplayerMultiplayerButtons(int yIn, int rowHeightIn)
	{
		this.addRenderableWidget(new Button(this.width / 2 - 100, yIn, 200, 20, Component.translatable("menu.singleplayer"), (p_213089_1_) ->
		{
			this.minecraft.setScreen(new SelectWorldScreen(this));
		}));
		this.addRenderableWidget(new Button(this.width / 2 - 100, yIn + rowHeightIn * 1, 200, 20, Component.translatable("menu.multiplayer"), (p_213086_1_) ->
		{
			this.minecraft.setScreen(new JoinMultiplayerScreen(this));
		}));
	}

	private void updatePackList(PackRepository p_241584_1_)
	{
		List<String> list = ImmutableList.copyOf(this.minecraft.options.resourcePacks);
		this.minecraft.options.resourcePacks.clear();
		this.minecraft.options.incompatibleResourcePacks.clear();

		for (Pack resourcepackinfo : p_241584_1_.getSelectedPacks())
		{
			if (!resourcepackinfo.isFixedPosition())
			{
				this.minecraft.options.resourcePacks.add(resourcepackinfo.getId());
				if (!resourcepackinfo.getCompatibility().isCompatible())
				{
					this.minecraft.options.incompatibleResourcePacks.add(resourcepackinfo.getId());
				}
			}
		}

		this.minecraft.options.save();
		List<String> list1 = ImmutableList.copyOf(this.minecraft.options.resourcePacks);
		if (!list1.equals(list))
		{
			this.minecraft.reloadResourcePacks();
		}

	}
}
