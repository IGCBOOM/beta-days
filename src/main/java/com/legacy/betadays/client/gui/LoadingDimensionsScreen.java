package com.legacy.betadays.client.gui;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;

public class LoadingDimensionsScreen extends Screen
{
	public boolean nether;

	public boolean leaving;

	public LoadingDimensionsScreen(boolean isNether, boolean leaving)
	{
		super(Component.empty());
		this.nether = isNether;
		this.leaving = leaving;
	}

	@Override
	public boolean shouldCloseOnEsc()
	{
		return false;
	}

	@Override
	public void render(PoseStack stack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderDirtBackground(0);
		drawCenteredString(stack, this.font, Component.translatable("menu.generatingTerrain"), this.width / 2, this.height / 2 - 30, 16777215);

		if (!leaving)
		{
			if (nether)
			{
				drawCenteredString(stack, this.font, Component.translatable("beta.menu.enterNether"), this.width / 2, this.height / 2 - 50, 16777215);
			}
			else
			{
				drawCenteredString(stack, this.font, Component.translatable("beta.menu.enterEnd"), this.width / 2, this.height / 2 - 50, 16777215);
			}
		}
		else
		{
			if (nether)
			{
				drawCenteredString(stack, this.font, Component.translatable("beta.menu.leaveNether"), this.width / 2, this.height / 2 - 50, 16777215);
			}
			else
			{
				drawCenteredString(stack, this.font, Component.translatable("beta.menu.leaveEnd"), this.width / 2, this.height / 2 - 50, 16777215);
			}
		}

		super.render(stack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean isPauseScreen()
	{
		return false;
	}
}