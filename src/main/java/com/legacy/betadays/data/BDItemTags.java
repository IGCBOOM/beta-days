package com.legacy.betadays.data;

import com.legacy.betadays.BetaDays;

import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;

public class BDItemTags
{
	public static final TagKey<Item> FOOD_STORAGE_UNSTACKABLES = tag("food_storage_unstackables");

	private static TagKey<Item> tag(String name)
	{
		return ItemTags.create(BetaDays.locate(name));
	}
}
