package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.betadays.BDConfig;

import net.minecraft.client.player.LocalPlayer;

@Mixin(LocalPlayer.class)
public class LocalPlayerMixin
{
	@Inject(at = @At("HEAD"), method = "hasEnoughImpulseToStartSprinting", cancellable = true)
	private void hasEnoughImpulseToStartSprinting(CallbackInfoReturnable<Boolean> callback)
	{
		// most consistant and stable way to stop sprinting without hacky FOV stuff
		if (BDConfig.SERVER.disableSprinting() && !((LocalPlayer) (Object) this).isCreative())
		{
			if (!BDConfig.SERVER.disableSwimming() && ((LocalPlayer) (Object) this).isUnderWater())
				callback.setReturnValue(true);
			else
				callback.setReturnValue(false);
		}
		else
		{
			if (BDConfig.SERVER.disableSwimming() && ((LocalPlayer) (Object) this).isUnderWater() && !((LocalPlayer) (Object) this).isCreative())
				callback.setReturnValue(false);
		}

	}
}
