package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.betadays.BDConfig;
import com.legacy.betadays.data.BDItemTags;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

@Mixin(ItemStack.class)
public class ItemStackMixin
{
	@Inject(at = @At("HEAD"), method = "getMaxStackSize", cancellable = true)
	private void getMaxStackSize(CallbackInfoReturnable<Integer> callback)
	{
		// changing stack size is a very awful thing to do, but since this is
		// configurable I don't mind it as much
		ItemStack stack = (ItemStack) (Object) this;
		if (BDConfig.SERVER.disableFoodStacking() && (stack.isEdible() || BDConfig.SERVER.makeFoodStorageUnstackable() && stack.is(BDItemTags.FOOD_STORAGE_UNSTACKABLES)))
			callback.setReturnValue(stack.getItem() == Items.COOKIE ? 8 : 1);
	}
}
