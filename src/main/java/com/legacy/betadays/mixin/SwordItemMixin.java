package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.betadays.BDConfig;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraftforge.common.ToolAction;

@Mixin(SwordItem.class)
public class SwordItemMixin
{
	@Inject(at = @At("HEAD"), method = "canPerformAction", cancellable = true, remap = false)
	private void canPerformAction(ItemStack stack, ToolAction toolAction, CallbackInfoReturnable<Boolean> callback)
	{
		// would think there would be a way to do this without resorting to mixin, missed it if there was
		if (BDConfig.SERVER.disableCombatSweep() && stack.getEnchantmentLevel(Enchantments.SWEEPING_EDGE) <= 0)
			callback.setReturnValue(false);
	}
}
