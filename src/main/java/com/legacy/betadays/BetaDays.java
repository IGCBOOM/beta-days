package com.legacy.betadays;

import com.legacy.betadays.client.BetaClientEvents;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(BetaDays.MODID)
public class BetaDays
{
	public static final String MODID = "beta_days";

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static String find(String key)
	{
		return MODID + ":" + key;
	}

	public BetaDays()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, BDConfig.SERVER_SPEC);

		var modBus = FMLJavaModLoadingContext.get().getModEventBus();

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, BDConfig.CLIENT_SPEC);

			MinecraftForge.EVENT_BUS.register(BetaClientEvents.class);
			modBus.addListener(BetaClientEvents::registerOverlays);
		});

		modBus.addListener(BetaDays::commonInit);
	}

	@SubscribeEvent
	public static void commonInit(FMLCommonSetupEvent event)
	{
		var forgeBus = MinecraftForge.EVENT_BUS;

		forgeBus.register(BetaPlayerEvents.class);

		/*for (Item item : ForgeRegistries.ITEMS.getValues())
		{
			if (BDConfig.SERVER.disableFoodStacking() && item.isEdible())
			{
				// TODO AT
				ObfuscationReflectionHelper.setPrivateValue(Item.class, item, item == Items.COOKIE ? 8 : 1, "f_41370_"); // maxStackSize
			}
		}*/
	}
}