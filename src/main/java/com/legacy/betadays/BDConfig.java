package com.legacy.betadays;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = BetaDays.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BDConfig
{

	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}

	public static class ClientConfig
	{
		private final ForgeConfigSpec.ConfigValue<Boolean> oldIngameVersion;

		private final ForgeConfigSpec.ConfigValue<Boolean> disableNetherFog;
		private final ForgeConfigSpec.ConfigValue<Boolean> customDimensionMessages;

		private final ForgeConfigSpec.ConfigValue<Boolean> enableClassicMenu;
		private final ForgeConfigSpec.ConfigValue<Boolean> disableCombatSounds;

		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Beta Days - Clientside configuration\n IF YOU ARE LOOKING FOR GAMEPLAY CHANGES, THEY ARE IN THE WORLD FOLDER.");

			builder.push("client");
			oldIngameVersion = builder.translation("oldIngameVersion").comment("Displays your Minecraft version at the top of screen when in game.").define("oldIngameVersion", false);

			disableNetherFog = builder.translation("disableNetherFog").comment("Removes the dense environmental fog from the Nether dimension, pushing back to the end of render distance").define("disableNetherFog", false);
			customDimensionMessages = builder.translation("customDimensionMessages").comment("Enable custom dimension entry messages. (Vanilla Dimensions)").define("customDimensionMessages", false);
			enableClassicMenu = builder.translation("enableClassicMenu").comment("Enable the classic menu. (May cause mod compatibility issues when used with other menu mods)").define("enableClassicMenu", false);
			disableCombatSounds = builder.translation("disableCombatSounds").comment("Disable the 1.9+ combat sounds.").define("disableCombatSounds", false);

			builder.pop();
		}

		public boolean displayIngameVersion()
		{
			return this.oldIngameVersion.get();
		}

		public boolean disableNetherFog()
		{
			return this.disableNetherFog.get();
		}

		public boolean customDimensionMessages()
		{
			return this.customDimensionMessages.get();
		}

		public boolean enableClassicMenu()
		{
			return this.enableClassicMenu.get();
		}

		public boolean disableCombatSounds()
		{
			return this.disableCombatSounds.get();
		}
	}

	public static class ServerConfig
	{
		private final ForgeConfigSpec.ConfigValue<Boolean> disableCombatCooldown;
		private final ForgeConfigSpec.ConfigValue<Boolean> hungerDisabled;
		private final ForgeConfigSpec.ConfigValue<Boolean> disableSprinting;
		private final ForgeConfigSpec.ConfigValue<Boolean> disableSwimming;
		private final ForgeConfigSpec.ConfigValue<Boolean> originalBow;
		private final ForgeConfigSpec.ConfigValue<Boolean> disableExperienceDrop;
		private final ForgeConfigSpec.ConfigValue<Boolean> disableFoodStacking;
		private final ForgeConfigSpec.ConfigValue<Boolean> makeFoodStorageUnstackable;
		private final ForgeConfigSpec.ConfigValue<Boolean> tillSeeds;
		private final ForgeConfigSpec.ConfigValue<Boolean> disableCombatSweep;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Beta Days - Gameplay configuration").push("world");
			disableCombatCooldown = builder.translation("disableCombatCooldown").comment("Disables the 1.9+ combat cooldown. (This also decreases axe damage)").define("disableCombatCooldown", false);
			hungerDisabled = builder.translation("hungerDisabled").comment("Disables hunger, food gives health instead. (Hides the hunger bar, putting armor in its place)").define("hungerDisabled", false);
			disableSprinting = builder.translation("disableSprinting").comment("Disables sprinting.").define("disableSprinting", false);
			disableSwimming = builder.translation("disableSwimming").comment("Disables swimming.").define("disableSwimming", false);
			originalBow = builder.translation("originalBow").comment("Allows for instantly shooting bows.").define("originalBow", false);
			disableExperienceDrop = builder.translation("disableExperienceDrop").comment("Disables mobs dropping experience. (Hides the experience bar along with it)").define("disableExperienceDrop", false);
			disableFoodStacking = builder.translation("disableFoodStacking").comment("Disables the ability to stack food items. When enabled, this also allows you to instantly eat food.").define("disableFoodStacking", false);
			makeFoodStorageUnstackable = builder.translation("makeFoodStorageUnstackable").comment("While food stacking is disabled, this will also make anything in the #beta_days:food_storage_unstackables tag all unstackable.").define("makeFoodStorageUnstackable", false);
			tillSeeds = builder.translation("tillSeeds").comment("Allows for seeds to drop randomly when tilling dirt.").define("tillSeeds", false);
			disableCombatSweep = builder.translation("disableCombatSweep").comment("Disables the 1.9+ sweep attack. Only applies when the Sweeping Edge enchantment is not used.").define("disableCombatSweep", false);

			builder.pop();
		}

		public boolean disableCombatCooldown()
		{
			return this.disableCombatCooldown.get();
		}

		public boolean hungerDisabled()
		{
			return this.hungerDisabled.get();
		}

		public boolean disableSprinting()
		{
			return this.disableSprinting.get();
		}

		public boolean disableSwimming()
		{
			return this.disableSwimming.get();
		}

		public boolean originalBow()
		{
			return this.originalBow.get();
		}

		public boolean disableExperienceDrop()
		{
			return this.disableExperienceDrop.get();
		}

		public boolean disableFoodStacking()
		{
			return this.disableFoodStacking.get();
		}

		public boolean makeFoodStorageUnstackable()
		{
			return this.makeFoodStorageUnstackable.get();
		}

		public boolean tillSeeds()
		{
			return this.tillSeeds.get();
		}

		public boolean disableCombatSweep()
		{
			return this.disableCombatSweep.get();
		}
	}
}
