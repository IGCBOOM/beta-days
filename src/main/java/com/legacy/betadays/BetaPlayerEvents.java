package com.legacy.betadays;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.CakeBlock;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingTickEvent;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.minecraftforge.event.entity.player.PlayerXpEvent;
import net.minecraftforge.event.level.BlockEvent.BlockToolModificationEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BetaPlayerEvents
{
	public static final AttributeModifier MAX_SPEED = new AttributeModifier("Beta Speed Modifier", Double.MAX_VALUE, Operation.ADDITION);

	@SubscribeEvent
	public static void onPlayerUpdate(LivingTickEvent event)
	{
		if (event.getEntity()instanceof Player player)
		{
			// Combat cooldown
			if (BDConfig.SERVER.disableCombatCooldown())
			{
				if (!player.getAttribute(Attributes.ATTACK_SPEED).hasModifier(MAX_SPEED))
					player.getAttribute(Attributes.ATTACK_SPEED).addTransientModifier(MAX_SPEED);
			}
			else if (player.getAttribute(Attributes.ATTACK_SPEED).hasModifier(MAX_SPEED))
				player.getAttribute(Attributes.ATTACK_SPEED).removeModifier(MAX_SPEED);

			// Make food always edible, but still allow sprinting if enabled
			if (BDConfig.SERVER.hungerDisabled() && player.getFoodData().getFoodLevel() != 7)
				player.getFoodData().setFoodLevel(7);

			// Reset air when surfacing while sprinting is disabled
			if (BDConfig.SERVER.disableSprinting() && BDConfig.SERVER.disableSwimming() && player.getAirSupply() < player.getMaxAirSupply() && !player.isEyeInFluidType(ForgeMod.WATER_TYPE.get()))
				player.setAirSupply(player.getMaxAirSupply());
			// Fixes being able to sprint or swim on discovered edge cases
			if (!player.isUnderWater() && BDConfig.SERVER.disableSprinting() && !player.isCreative())
				player.setSprinting(false);
			if (player.isUnderWater() && BDConfig.SERVER.disableSwimming() && !player.isCreative())
				player.setSprinting(false);
		}
	}

	@SubscribeEvent
	public static void onRightClickBlock(RightClickBlock event)
	{
		if (BDConfig.SERVER.hungerDisabled() && event.getUseBlock() != Result.ALLOW && event.getLevel().getBlockState(event.getPos()).getBlock() instanceof CakeBlock)
			event.getEntity().heal(2.0F);
	}

	@SubscribeEvent
	public static void onPlayerRightClick(RightClickItem event)
	{
		ItemStack item = event.getItemStack();
		Player player = event.getEntity();

		if (BDConfig.SERVER.hungerDisabled() && item.getItem().isEdible())
		{
			FoodProperties food = item.getFoodProperties(player);

			// Prevent eating if at full health unless specifically set otherwise
			if (player.getHealth() >= player.getMaxHealth() && !food.canAlwaysEat())
			{
				event.setCanceled(true);
			}
			else if (BDConfig.SERVER.disableFoodStacking())
			{
				// eat the item right away
				player.setItemInHand(event.getHand(), item.finishUsingItem(player.level, player));
				player.heal(food.getNutrition());

				event.setCanceled(true);
			}
		}

		if (BDConfig.SERVER.originalBow())
		{
			if (item.getItem()instanceof BowItem bow)
			{
				bow.releaseUsing(item, player.level, player, 200);
				event.setCanceled(true);
			}

			if (item.getItem().getClass().getName().equals("mod.torchbowmod.TorchBow"))
			{
				item.finishUsingItem(player.level, player);
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public static void onEntityXPDrop(LivingExperienceDropEvent event)
	{
		if (BDConfig.SERVER.disableExperienceDrop())
			event.setCanceled(true);
	}

	@SubscribeEvent
	public static void onEntityXPPickup(PlayerXpEvent.PickupXp event)
	{
		// Failsafe
		if (BDConfig.SERVER.disableExperienceDrop())
		{
			event.getOrb().discard();
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public static void onEntityDamaged(LivingDamageEvent event)
	{
		if (BDConfig.SERVER.disableCombatCooldown() && event.getSource().getDirectEntity()instanceof LivingEntity living && living.getMainHandItem().getItem() instanceof AxeItem)
			event.setAmount(event.getAmount() * 0.5F);
	}

	@SubscribeEvent
	public static void onItemFinishUse(LivingEntityUseItemEvent.Finish event)
	{
		if (BDConfig.SERVER.hungerDisabled())
		{
			if (!BDConfig.SERVER.disableFoodStacking() && event.getEntity()instanceof Player player && event.getItem().isEdible())
			{
				FoodProperties food = event.getItem().getFoodProperties(player);

				event.getItem().getItem().finishUsingItem(event.getItem(), player.level, player);
				event.getEntity().heal(food.getNutrition());
			}

			if (event.getItem().getItem() == Items.ROTTEN_FLESH && event.getEntity().getRandom().nextBoolean())
				event.getEntity().addEffect(new MobEffectInstance(MobEffects.POISON, 60));
		}
	}

	@SubscribeEvent
	public static void onToolUse(final BlockToolModificationEvent event)
	{
		LevelAccessor level = event.getLevel();
		if (event.getToolAction() == ToolActions.HOE_TILL)
		{
			if (BDConfig.SERVER.tillSeeds() && event.getState().is(BlockTags.DIRT))
			{
				if (level.getRandom().nextInt(10) == 0)
				{
					BlockPos pos = event.getContext().getClickedPos();
					ItemEntity itementity = new ItemEntity(event.getPlayer().level, pos.getX() + 0.5F, pos.getY() + 1, pos.getZ() + 0.5F, new ItemStack(Items.WHEAT_SEEDS));
					itementity.setDefaultPickUpDelay();
					level.addFreshEntity(itementity);
				}
			}
		}
	}
}
